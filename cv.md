# Marc Riera

**Data de naixement:** 22/01/2002

**Nacionalitat:** Espanyola

**Carrer:** Travessera de Dalt 110 1r 2a

**Mobil** 640-355-884

**Correu electronic:** marcriera2002@gmail.com



![fotografia](/images/foto.jpg)

| Estudis | Habilitats |
| ----------- | ----------- |
| ESO 2014-2018 Institut la Sedeta | **Llenguatges de programacio**|
| Batxillerat Tecnologic 2018-2020 Institut la Sedeta | Java|
| DAM 2020-2022 Institut Tecnologic de Barcelona | Python |
| **Llenguas** | C# |
| Catala  | **Programes**|
| Castella | Intelij Idea |
| Angles | Git |
| | Linux |
| | Windows |
| | Excel |

## Com soc?
Soc una persona amb moltes ganes de aprendre noves habilitats, tinc molta paciencia i sempre intento ajudar el maxim possible.

Com he estat amb un ordinador desde petit, se solucionar varios problemes amb que poden ocasionar, buscar jo mateix les solucions i cuidar-los perque no passin.